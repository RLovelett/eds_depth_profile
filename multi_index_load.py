# coding: utf-8

import numpy as np
import pandas as pd
import prz_fit_fncs as pr

MW_Ag = 107.8682
MW_Cu = 63.546
MW_In = 114.82
MW_Ga = 69.723
MW_Se = 78.96
MW_S = 32.065


Ga_list = [0.002, 0.025, 0.050, 0.100,0.125,0.150, 0.175, 0.200, 0.225 ,0.248]
Ga_conc = []
for element in Ga_list:
    Ga_conc += [5.7*element*MW_Ga/(0.075*MW_Ag + 
                              0.175*MW_Cu + 
                              (0.25-element)*MW_In + 
                              element*MW_Ga +
                              0.5*MW_Se)]

e0_list = [5, 10, 15, 16, 17, 18, 19, 20, 22, 25, 27, 30]
var_e0_df = []
for e0 in ['5', '10', '15', '16', '17', '18', '19', '20','22', '25', '27', '30']: 
    var_Ga_df = []
    for Ga in ['0.002', '0.025', '0.050', '0.100','0.125','0.150', '0.175', '0.200', '0.225' ,'0.248']:
        var_Ga_df += [pr.PRZ_DF('standards/CIGS_at_'+e0+'.0_keV_'+Ga+'/PhiRhoZ.txt',3)]
    
    dict_of_df = dict(zip(Ga_conc, var_Ga_df))
    var_e0_df += [pd.concat(dict_of_df, copy=True)]
    del dict_of_df
    del var_Ga_df

dict_of_df = dict(zip(e0_list, var_e0_df))
df_prz_Ga_e0 = pd.concat(dict_of_df, copy = True)
del dict_of_df
del var_e0_df
