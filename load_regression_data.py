# coding: utf-8

# In[1]:

import numpy as np
import scipy.optimize as spo
import scipy.integrate as spi
import bokeh.plotting as bkp
import pandas as pd
bkp.output_notebook()

prz_20_0_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.002/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_20_0_Ga['Min']
del prz_20_0_Ga['Max']
prz_20_0_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_20_1_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.025/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_20_1_Ga['Min']
del prz_20_1_Ga['Max']
prz_20_1_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_20_2_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.050/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_20_2_Ga['Min']
del prz_20_2_Ga['Max']
prz_20_2_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_20_3_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.075/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_20_3_Ga['Min']
del prz_20_3_Ga['Max']
prz_20_3_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_20_4_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.100/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_20_4_Ga['Min']
del prz_20_4_Ga['Max']
prz_20_4_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_20_5_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.125/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_20_5_Ga['Min']
del prz_20_5_Ga['Max']
prz_20_5_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_20_6_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.150/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_20_6_Ga['Min']
del prz_20_6_Ga['Max']
prz_20_6_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_20_7_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.175/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_20_7_Ga['Min']
del prz_20_7_Ga['Max']
prz_20_7_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_20_8_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.200/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_20_8_Ga['Min']
del prz_20_8_Ga['Max']
prz_20_8_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_20_9_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.225/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_20_9_Ga['Min']
del prz_20_9_Ga['Max']
prz_20_9_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_20_10_Ga = pd.read_table('standards/CIGS_at_20.0_keV_0.248/PhiRhoZ.txt',
                          comment = '\t', dtype = np.float64)
del prz_20_10_Ga['Min']
del prz_20_10_Ga['Max']
prz_20_10_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)

dict_of_df = {0.01 : prz_20_0_Ga,
               0.1 : prz_20_1_Ga,
               0.2 : prz_20_2_Ga,
               0.3 : prz_20_3_Ga,
               0.4 : prz_20_4_Ga,
               0.5 : prz_20_5_Ga,
               0.6 : prz_20_6_Ga,
               0.7 : prz_20_7_Ga,
               0.8 : prz_20_8_Ga,
               0.9 : prz_20_9_Ga,
              0.99 : prz_20_10_Ga}


panel_20keV = pd.Panel(data = dict_of_df, copy = True)
del dict_of_df
del prz_20_0_Ga
del prz_20_1_Ga
del prz_20_2_Ga
del prz_20_3_Ga
del prz_20_4_Ga
del prz_20_5_Ga
del prz_20_6_Ga
del prz_20_7_Ga
del prz_20_8_Ga
del prz_20_9_Ga
del prz_20_10_Ga

# 19 keV
prz_19_0_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.002/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_19_0_Ga['Min']
del prz_19_0_Ga['Max']
prz_19_0_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_19_1_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.025/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_19_1_Ga['Min']
del prz_19_1_Ga['Max']
prz_19_1_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_19_2_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.050/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_19_2_Ga['Min']
del prz_19_2_Ga['Max']
prz_19_2_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_19_3_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.075/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_19_3_Ga['Min']
del prz_19_3_Ga['Max']
prz_19_3_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_19_4_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.100/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_19_4_Ga['Min']
del prz_19_4_Ga['Max']
prz_19_4_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_19_5_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.125/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_19_5_Ga['Min']
del prz_19_5_Ga['Max']
prz_19_5_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_19_6_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.150/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_19_6_Ga['Min']
del prz_19_6_Ga['Max']
prz_19_6_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_19_7_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.175/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_19_7_Ga['Min']
del prz_19_7_Ga['Max']
prz_19_7_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_19_8_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.200/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_19_8_Ga['Min']
del prz_19_8_Ga['Max']
prz_19_8_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_19_9_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.225/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_19_9_Ga['Min']
del prz_19_9_Ga['Max']
prz_19_9_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_19_10_Ga = pd.read_table('standards/CIGS_at_19.0_keV_0.248/PhiRhoZ.txt',
                          comment = '\t', dtype = np.float64)
del prz_19_10_Ga['Min']
del prz_19_10_Ga['Max']
prz_19_10_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)

dict_of_df = {0.01 : prz_19_0_Ga,
               0.1 : prz_19_1_Ga,
               0.2 : prz_19_2_Ga,
               0.3 : prz_19_3_Ga,
               0.4 : prz_19_4_Ga,
               0.5 : prz_19_5_Ga,
               0.6 : prz_19_6_Ga,
               0.7 : prz_19_7_Ga,
               0.8 : prz_19_8_Ga,
               0.9 : prz_19_9_Ga,
              0.99 : prz_19_10_Ga}


panel_19keV = pd.Panel(data = dict_of_df, copy = True)
del dict_of_df
del prz_19_0_Ga
del prz_19_1_Ga
del prz_19_2_Ga
del prz_19_3_Ga
del prz_19_4_Ga
del prz_19_5_Ga
del prz_19_6_Ga
del prz_19_7_Ga
del prz_19_8_Ga
del prz_19_9_Ga
del prz_19_10_Ga
## for 15 keV

prz_15_0_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.002/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_15_0_Ga['Min']
del prz_15_0_Ga['Max']
prz_15_0_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_15_1_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.025/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_15_1_Ga['Min']
del prz_15_1_Ga['Max']
prz_15_1_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_15_2_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.050/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_15_2_Ga['Min']
del prz_15_2_Ga['Max']
prz_15_2_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_15_3_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.075/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_15_3_Ga['Min']
del prz_15_3_Ga['Max']
prz_15_3_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_15_4_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.100/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_15_4_Ga['Min']
del prz_15_4_Ga['Max']
prz_15_4_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_15_5_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.125/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_15_5_Ga['Min']
del prz_15_5_Ga['Max']
prz_15_5_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_15_6_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.150/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_15_6_Ga['Min']
del prz_15_6_Ga['Max']
prz_15_6_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_15_7_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.175/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_15_7_Ga['Min']
del prz_15_7_Ga['Max']
prz_15_7_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_15_8_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.200/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_15_8_Ga['Min']
del prz_15_8_Ga['Max']
prz_15_8_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_15_9_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.225/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_15_9_Ga['Min']
del prz_15_9_Ga['Max']
prz_15_9_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_15_10_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.248/PhiRhoZ.txt',
                          comment = '\t', dtype = np.float64)
del prz_15_10_Ga['Min']
del prz_15_10_Ga['Max']
prz_15_10_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


dict_of_df = {0.01 : prz_15_0_Ga,
               0.1 : prz_15_1_Ga,
               0.2 : prz_15_2_Ga,
               0.3 : prz_15_3_Ga,
               0.4 : prz_15_4_Ga,
               0.5 : prz_15_5_Ga,
               0.6 : prz_15_6_Ga,
               0.7 : prz_15_7_Ga,
               0.8 : prz_15_8_Ga,
               0.9 : prz_15_9_Ga,
              0.99 : prz_15_10_Ga}
panel_15keV = pd.Panel(data = dict_of_df, copy = True)
del dict_of_df
del prz_15_0_Ga
del prz_15_1_Ga
del prz_15_2_Ga
del prz_15_3_Ga
del prz_15_4_Ga
del prz_15_5_Ga
del prz_15_6_Ga
del prz_15_7_Ga
del prz_15_8_Ga
del prz_15_9_Ga
del prz_15_10_Ga

## 10 keV

# In[6]:

prz_10_0_Ga = pd.read_table('standards/CIGS_at_10.0_keV_0.002/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_10_0_Ga['Min']
del prz_10_0_Ga['Max']
prz_10_0_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_10_1_Ga = pd.read_table('standards/CIGS_at_10.0_keV_0.025/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_10_1_Ga['Min']
del prz_10_1_Ga['Max']
prz_10_1_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_10_2_Ga = pd.read_table('standards/CIGS_at_10.0_keV_0.050/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_10_2_Ga['Min']
del prz_10_2_Ga['Max']
prz_10_2_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_10_3_Ga = pd.read_table('standards/CIGS_at_10.0_keV_0.075/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_10_3_Ga['Min']
del prz_10_3_Ga['Max']
prz_10_3_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_10_4_Ga = pd.read_table('standards/CIGS_at_10.0_keV_0.100/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_10_4_Ga['Min']
del prz_10_4_Ga['Max']
prz_10_4_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_10_5_Ga = pd.read_table('standards/CIGS_at_10.0_keV_0.125/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_10_5_Ga['Min']
del prz_10_5_Ga['Max']
prz_10_5_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_10_6_Ga = pd.read_table('standards/CIGS_at_10.0_keV_0.150/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_10_6_Ga['Min']
del prz_10_6_Ga['Max']
prz_10_6_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_10_7_Ga = pd.read_table('standards/CIGS_at_10.0_keV_0.175/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_10_7_Ga['Min']
del prz_10_7_Ga['Max']
prz_10_7_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_10_8_Ga = pd.read_table('standards/CIGS_at_10.0_keV_0.200/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_10_8_Ga['Min']
del prz_10_8_Ga['Max']
prz_10_8_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_10_9_Ga = pd.read_table('standards/CIGS_at_15.0_keV_0.225/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_10_9_Ga['Min']
del prz_10_9_Ga['Max']
prz_10_9_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_10_10_Ga = pd.read_table('standards/CIGS_at_10.0_keV_0.248/PhiRhoZ.txt',
                          comment = '\t', dtype = np.float64)
del prz_10_10_Ga['Min']
del prz_10_10_Ga['Max']
prz_10_10_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


dict_of_df = {0.01 : prz_10_0_Ga,
               0.1 : prz_10_1_Ga,
               0.2 : prz_10_2_Ga,
               0.3 : prz_10_3_Ga,
               0.4 : prz_10_4_Ga,
               0.5 : prz_10_5_Ga,
               0.6 : prz_10_6_Ga,
               0.7 : prz_10_7_Ga,
               0.8 : prz_10_8_Ga,
               0.9 : prz_10_9_Ga,
              0.99 : prz_10_10_Ga}
panel_10keV = pd.Panel(data = dict_of_df, copy = True)
del dict_of_df
del prz_10_0_Ga
del prz_10_1_Ga
del prz_10_2_Ga
del prz_10_3_Ga
del prz_10_4_Ga
del prz_10_5_Ga
del prz_10_6_Ga
del prz_10_7_Ga
del prz_10_8_Ga
del prz_10_9_Ga
del prz_10_10_Ga

## 5keV

# In[7]:

prz_5_0_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.002/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_5_0_Ga['Min']
del prz_5_0_Ga['Max']
prz_5_0_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_5_1_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.025/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_5_1_Ga['Min']
del prz_5_1_Ga['Max']
prz_5_1_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_5_2_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.050/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_5_2_Ga['Min']
del prz_5_2_Ga['Max']
prz_5_2_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_5_3_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.075/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_5_3_Ga['Min']
del prz_5_3_Ga['Max']
prz_5_3_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_5_4_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.100/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_5_4_Ga['Min']
del prz_5_4_Ga['Max']
prz_5_4_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_5_5_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.125/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_5_5_Ga['Min']
del prz_5_5_Ga['Max']
prz_5_5_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_5_6_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.150/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_5_6_Ga['Min']
del prz_5_6_Ga['Max']
prz_5_6_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_5_7_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.175/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_5_7_Ga['Min']
del prz_5_7_Ga['Max']
prz_5_7_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_5_8_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.200/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_5_8_Ga['Min']
del prz_5_8_Ga['Max']
prz_5_8_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_5_9_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.225/PhiRhoZ.txt',
                         comment = '\t', dtype = np.float64)
del prz_5_9_Ga['Min']
del prz_5_9_Ga['Max']
prz_5_9_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


prz_5_10_Ga = pd.read_table('standards/CIGS_at_5.0_keV_0.248/PhiRhoZ.txt',
                          comment = '\t', dtype = np.float64)
del prz_5_10_Ga['Min']
del prz_5_10_Ga['Max']
prz_5_10_Ga.index = np.linspace(-.1*3, 1.1*3, num = 110)


dict_of_df = {0.01 : prz_5_0_Ga,
               0.1 : prz_5_1_Ga,
               0.2 : prz_5_2_Ga,
               0.3 : prz_5_3_Ga,
               0.4 : prz_5_4_Ga,
               0.5 : prz_5_5_Ga,
               0.6 : prz_5_6_Ga,
               0.7 : prz_5_7_Ga,
               0.8 : prz_5_8_Ga,
               0.9 : prz_5_9_Ga,
              0.99 : prz_5_10_Ga}
panel_5keV = pd.Panel(data = dict_of_df, copy = True)
del dict_of_df
del prz_5_0_Ga
del prz_5_1_Ga
del prz_5_2_Ga
del prz_5_3_Ga
del prz_5_4_Ga
del prz_5_5_Ga
del prz_5_6_Ga
del prz_5_7_Ga
del prz_5_8_Ga
del prz_5_9_Ga
del prz_5_10_Ga



