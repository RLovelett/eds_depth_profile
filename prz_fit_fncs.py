# coding: utf-8

import numpy as np
import scipy.optimize as spo
import scipy.integrate as spi
import bokeh.plotting as bkp
import pandas as pd


def prz(z, phi_0, phi_m, z_m, z_x):
    """
    ### Analytical approximation of $\Phi(z)$ plot using 4 adjustable parameters 
    ($\Phi_0, \Phi_m, z_m, z_x$):<br />
    $\Phi(z \in (0, z_m)) = \Phi_m \exp{(-(z-z_m)^2/\beta^2)}$ <br \>
    $\Phi(z \in (z_m, z_x)) = \Phi_m \exp{(-(z-z_m)^2/\alpha^2)}$ <br \> 
    $\beta = z_m \ln{\Phi_m/\Phi_0}^{-1/2}$ and $\alpha = \frac{(z_x-z_m)}{\sqrt{-ln{(0.01/\Phi_m)}}}$
    """
    #assert z >= 0, 'position must be greater than or equal to zero'
    #    return phi_0
    import pdb

    #pdb.set_trace()
    z = np.array(z)
    phi_0 = np.array(phi_0)
    phi_m = np.array(phi_m)
    z_m = np.array(z_m)
    z_x = np.array(z_x)

    beta = z_m*np.log(phi_m/phi_0)**(-0.5)
    alpha = (z_x - z_m)*np.sqrt(-(np.log(0.01/phi_m)**-1.))
    def left_gaussian(z,phi_m,z_m,alpha,beta):
        return phi_m * np.exp(-(z-z_m)**2./beta**2.)
    def right_gaussian(z,phi_m,z_m,alpha, beta):
        return phi_m * np.exp(-(z-z_m)**2./alpha**2.)
    def Heaviside(z):
        return 0.5 * (np.sign(z)+1)
    return np.nan_to_num(Heaviside(z_m-z)*left_gaussian(z,phi_m,z_m,alpha,beta)+
            Heaviside(z-z_m)*right_gaussian(z,phi_m,z_m,alpha,beta))
    #if  z < z_m:
    #    beta = z_m*np.log(phi_m/phi_0)**(-0.5)#z_m*np.log(phi_m)/np.sqrt(phi_0)
    #    return phi_m * np.exp(-(z-z_m)**2/beta**2)
    #elif z == z_m:
    #    return phi_m
    #elif z > z_m:
    #    alpha = (z_x - z_m)*np.sqrt(-(np.log(0.01/phi_m)**-1))
    #    return phi_m * np.exp(-(z-z_m)**2/alpha**2)

#def Ga_wt_to_at_frac(Ga_wt_frac):
#    import scipy.optimize as spo
#    import pdb
#    wt_Ag = 107.8682
#    wt_Cu = 63.546
#    wt_In = 114.82
#    wt_Ga = 69.723
#    wt_Se = 78.96
#    wt_S = 32.065
#    def fun(params):
#        [x_Ga, Ag_wt_frac, Cu_wt_frac, In_wt_frac, Se_wt_frac]=params
#        denom = (Ag_wt_frac/wt_Ag+ 
#                 Cu_wt_frac/wt_Cu +
#                 In_wt_frac/wt_In +
#                 Ga_wt_frac/wt_Ga +
#                 Se_wt_frac/wt_Se)
#        f1 = x_Ga - Ga_wt_frac/wt_Ga/denom
#        f2 = 0.075 - Ag_wt_frac/wt_Ag/denom
#        f3 = 0.175 - Cu_wt_frac/wt_Cu/denom
#        f4 = 0.5 - Se_wt_frac/wt_Se/denom
#        f5 = 0.25 - x_Ga - In_wt_frac/wt_In/denom
#        return np.array([f1, f2, f3, f4, f5])
#
#    x = spo.fsolve(fun, 0.2*np.ones(5))
#    #pdb.set_trace()
#    return x[0]

def resid_norm(prz_dat, params):
    """
    Given data and parameters, calculates the norm of the residual between the model and the data
    """
    resid = np.ones(len(prz_dat))
    #print params
    for i in range(len(prz_dat.index)):
        resid[i] = (prz_dat.iloc[i] - prz(prz_dat.index[i], params[0], params[1], params[2], params[3]))
    #print np.linalg.norm(resid)
    return np.linalg.norm(resid)
                 
# Given polynomials for each of the parameters, gets prz as function of z and Ga
def general_prz(z, Ga, phi_0_fit, phi_m_fit, z_m_fit, z_x_fit):
    phi_0_Ga = np.polyval(phi_0_fit, Ga)
    phi_m_Ga = np.polyval(phi_m_fit, Ga)
    z_m_Ga = np.polyval(z_m_fit, Ga)
    z_x_Ga = np.polyval(z_x_fit, Ga)
    return prz(z,phi_0_Ga, phi_m_Ga, z_m_Ga, z_x_Ga)

def two_way_general_prz(z, e0, Ga_conc, phi_0_predictor, phi_m_predictor, z_m_predictor, z_x_predictor):
    """
    z is z value in um
    e0 is acc. voltage in keV
    Ga_conc is atom fraction GA (NOT GA/III)
    *_predictor are the ols predictors from get_predictor function
    """
    phi_0 = phi_0_predictor(exog=dict({'e0':e0,'Ga_conc':Ga_conc}))
    phi_m = phi_m_predictor(exog=dict({'e0':e0,'Ga_conc':Ga_conc,}))
    z_m = z_m_predictor(exog=dict({'e0':e0,'Ga_conc':Ga_conc}))
    z_x = z_x_predictor(exog=dict({'e0':e0,'Ga_conc':Ga_conc}))
    return prz(z,phi_0, phi_m, z_m, z_x)

def PRZ_DF(prz_file, prz_length):
    df = pd.read_table(prz_file, comment = '\t', dtype = np.float64)
    del df['Min']
    del df['Max']
    df.index = np.linspace(-0.1*prz_length, 1.1*prz_length, 110)

    return df.copy()
def two_way_fit(multi_DF, line = 'Emit_I[Ga K-L2]'):
    """
    multi_Df is a multiple indexed DF with position, Ga at fraction, and e0 as indices.

    line is the x-ray line to fit the params to--should be non-normalized to vary properly with the independent variables

    Function will return polynomials fits of the fitted Phi_0, Phi_m, z_m, and z_x variables as a function of the independent item label.
    """
    import statsmodels.formula.api as smf
    import pdb

    results_df = pd.DataFrame(columns = ['e0', 
                                         'Ga_conc',
                                         'Phi_0',
                                         'Phi_m',
                                         'z_m',
                                         'z_x'])
    #### Optimize to Fit Parameters
    
    for i, e0 in enumerate(multi_DF.index.levels[0]):
        print e0
        for j, Ga_conc in enumerate(multi_DF.index.levels[1]):
            #print Ga_conc
            prz_dat = multi_DF.loc[e0,Ga_conc][line][multi_DF.loc[e0,Ga_conc][line].index>=-0.03].copy()
            if np.isnan(prz_dat.iloc[0]):
                break
            # Get initial guess for optimization routine
            params_0 = np.array([prz_dat.iloc[1], 
                                max(prz_dat), 
                                prz_dat[prz_dat == max(prz_dat)].index.tolist()[0],
                                prz_dat[prz_dat <= 0.05*max(prz_dat)].index.tolist()[0]])
            #pdb.set_trace()
            res = spo.minimize(lambda params : resid_norm(prz_dat, params),
                       params_0,
                       method = 'COBYLA',
                       constraints = [
                                     dict({
                                     'type' : 'ineq',
                                     'fun' : lambda params : params[1] - params[0]})
                                     ],
                       bounds = [(0, None), (0, None), (-3.3, 3.3), (0,3.3)])
            results_df = results_df.append(pd.DataFrame(
                                              dict({'e0':e0,
                                                    'Ga_conc':Ga_conc,
                                                    'Phi_0':res.x[0],
                                                    'Phi_m':res.x[1],
                                                    'z_m':res.x[2],
                                                    'z_x':res.x[3]}),
                                              index = [0]),
                                           ignore_index=True)
            print res.x
            #pdb.set_trace()
    #results_df['e02'] = results_df.e0**2
    #results_df['Ga_conc2'] = results_df.Ga_conc**2
    lsq_Phi_0 = smf.ols(formula = 'Phi_0 ~ e0 + I(e0**2.) + I(e0**3.) + Ga_conc + I(Ga_conc**2.) + e0:Ga_conc',
                  data = results_df)
    lsq_Phi_m = smf.ols(formula = 'Phi_m ~ e0 + I(e0**2.) + I(e0**3.) + Ga_conc + I(Ga_conc**2.) + e0:Ga_conc',
                  data = results_df)
    lsq_z_m = smf.ols(formula = 'z_m ~ e0 + I(e0**2.) + I(e0**3.) + Ga_conc + I(Ga_conc**2.) + e0:Ga_conc',
                  data = results_df)
    lsq_z_x = smf.ols(formula = 'z_x ~ e0 + I(e0**2.) + I(e0**3.) + Ga_conc + I(Ga_conc**2.) + e0:Ga_conc',
                  data = results_df)
    pdb.set_trace()
    return (lsq_Phi_0, lsq_Phi_m, lsq_z_m, lsq_z_x, results_df)

def get_fit(line = 'Emit_I[Ga K-L2]'):
    """
    Function will get the ols predictor objects used to predict
    phi(z) parameters (phi_0, phi_m, z_m, z_x) as 
    functions of Ga at and e0.

    Model form: Param = B0+ B1*e0 + B2*Ga_conc + B3*e0*Ga_conc
    """
    import pdb
    from multi_index_load import df_prz_Ga_e0

    (Phi_0_mod, Phi_m_mod, z_m_mod, z_x_mod, df) = two_way_fit(df_prz_Ga_e0, line = line)

    return (Phi_0_mod.fit(),
            Phi_m_mod.fit(),
            z_m_mod.fit(),
            z_x_mod.fit())

def two_way_plot_test(phi_0_predictor, phi_m_predictor, z_m_predictor, z_x_predictor,
                      e0, MC_prz_df, line, profile, output='out.html'):
    """
    plot the prz from this method against an MC simulated prz
    *_fit are the ols predictors from the get_predictor function (which calls the optimization routine to get parameters at each e0, Ga_conc)
    MC_prz_df is the MC simulated prz (in the standard df form)
    line is the x-ray of interest
    profile is a function that returns the Ga_conc as a function of z
    output is the name to save the output to (html file)
    """
    import pdb
    bkp.output_file(output)
    z_fit = np.linspace(0, max(MC_prz_df.index), 100)
    phi_fit = np.empty(100)
    Ga = np.empty(100)
    for i in range(len(phi_fit)):
        Ga[i] = profile(z_fit[i])
        phi_fit[i] = two_way_general_prz(z_fit[i], e0, Ga[i],
                              phi_0_predictor, phi_m_predictor,
                              z_m_predictor, z_x_predictor)

    #pdb.set_trace()
    fig1 = bkp.figure(plot_width = 800, plot_height = 400,
                      tools = ['save', 'box_zoom', 'reset'],
                      title = line+', e0='+str(e0)+'keV', x_axis_label = 'z [μm]',
                      y_axis_label = 'Φ [1/(m)]', x_range = (0,3))

    fig1.line(x = z_fit, y = phi_fit,line_width = 1,
             color = 'black', legend = 'Analytical')
    fig1.triangle(x = z_fit, y = phi_fit, size = 4,
                  color = 'black', legend = 'Analytical')
    fig1.line(x = MC_prz_df.index, y = MC_prz_df[line], line_width = 1,
              color = 'black', legend = 'MC Simulation')
    fig1.circle(x = MC_prz_df.index, y = MC_prz_df[line], size = 2,
              color = 'black', legend = 'MC Simulation')
              

    fig2 = bkp.figure(plot_width = 800, plot_height = 400,
                      tools = ['save', 'box_zoom', 'reset'],
                      title = 'Ga Profile', x_axis_label = 'z [μm]',
                      y_axis_label = 'Ga/III', x_range = (0,3))
    fig2.line(x = z_fit, y = Ga, linewidth = 3, color = 'black')
 
    bkp.save(bkp.gridplot([[fig1],[fig2]]))

    return (fig1, fig2)
def params_fit(standard_panel, line = 'Emit_I[Ga K-L2]'):
    """
    standard_panel is a pandas panel organized such that the items are PRZ dataframes from homogenous standards
    (loaded using PRZ_DF function above) that are labeled by an independent variable/regressor (likely Ga/III or acc V)

    line is the x-ray line to fit the params to--should be non-normalized to vary properly with the exogenous variable

    Function will return polynomials fits of the fitted Phi_0, Phi_m, z_m, and z_x variables as a function of the independent item label.
    """
    #### Optimize to Fit Parameters
    params_fit = np.empty([len(standard_panel.items), 4])
    params_0 = np.empty([len(standard_panel.items),4])
    for i, indep_var in enumerate(standard_panel.items):
        

        prz_dat = standard_panel[indep_var][line].loc[standard_panel[indep_var].index>=0].copy()
        # Get initial guess for optimization routine
        params_0[i] = np.array([prz_dat.iloc[0], 
                                max(prz_dat), 
                                prz_dat[prz_dat == max(prz_dat)].index.tolist()[0],
                                prz_dat[prz_dat <= 0.05*max(prz_dat)].index.tolist()[0]])
        res = spo.minimize(lambda params : resid_norm(prz_dat, params),
                       params_0[i],
                       bounds = [(0, None), (0, None), (-3.3, 3.3), (0,3.3)])
        params_fit[i] = res.x
    
    phi_0_fit = np.polyfit(standard_panel.items, params_fit.T[0], 1)
    phi_m_fit = np.polyfit(standard_panel.items, params_fit.T[1], 1)
    z_m_fit = np.polyfit(standard_panel.items, params_fit.T[2], 2)
    z_x_fit = np.polyfit(standard_panel.items, params_fit.T[3], 2)

    return (phi_0_fit, phi_m_fit, z_m_fit, z_x_fit)

def mdl_vs_sim_plot(phi_0_fit, phi_m_fit, z_m_fit, z_x_fit,
                    MC_prz_df, line, profile, output='out.html'):
    """
    plot the prz from this method against an MC simulated prz
    *_fit are the polynomial fits from the optimization routine
    MC_prz_df is the MC simulated prz (in the standard df form)
    line is the x-ray of interest
    profile is a function that returns the Ga_III as a function of z
    output is the name to save the output to (html file)
    """
    import pdb
    bkp.output_file(output)
    z_fit = np.linspace(0, max(MC_prz_df.index), 100)
    phi_fit = np.empty(100)
    Ga = np.empty(100)
    for i in range(len(phi_fit)):
        Ga[i] = profile(z_fit[i])
        phi_fit[i] = general_prz(z_fit[i], Ga[i],
                              phi_0_fit, phi_m_fit,
                              z_m_fit, z_x_fit)

    #pdb.set_trace()
    fig1 = bkp.figure(plot_width = 800, plot_height = 400,
                      tools = ['save', 'box_zoom', 'reset'],
                      title = line, x_axis_label = 'z [μm]',
                      y_axis_label = 'Φ [1/(m)]', x_range = (0,3))

    fig1.line(x = z_fit, y = phi_fit,line_width = 1,
             color = 'black', legend = 'Analytical')
    fig1.triangle(x = z_fit, y = phi_fit, size = 4,
                  color = 'black', legend = 'Analytical')
    fig1.line(x = MC_prz_df.index, y = MC_prz_df[line], line_width = 1,
              color = 'black', legend = 'MC Simulation')
    fig1.circle(x = MC_prz_df.index, y = MC_prz_df[line], size = 2,
              color = 'black', legend = 'MC Simulation')
              

    fig2 = bkp.figure(plot_width = 800, plot_height = 400,
                      tools = ['save', 'box_zoom', 'reset'],
                      title = 'Ga Profile', x_axis_label = 'z [μm]',
                      y_axis_label = 'Ga/III', x_range = (0,3))
    fig2.line(x = z_fit, y = Ga, linewidth = 3, color = 'black')
 
    bkp.save(bkp.gridplot([[fig1],[fig2]]))

    return (fig1, fig2)

def two_line_profile(z, point_z, point_Ga, slope1, slope2):
    """
    function to provide a two line profile given the point and slopes
    of each of the lines.
    inputs are:
    z, the position
    point_z, the location of intersection (minimum, typically)
    point_Ga, the value of Ga/III at point_z (minimum, typically)
    slope1, slope of the 1st line
    slope2, slope of the 2nd line
    """
    
    if z == point_z:
        return  point_Ga
    elif z < point_z:
        return slope1*(z - point_z) + point_Ga
    elif z > point_z:
        return slope2*(z - point_z) + point_Ga


