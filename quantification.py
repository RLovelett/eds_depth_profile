"""
Module to contain spectrum class with various quantification methods
available. Different weigting, background stripping, standards to use, etc.
"""
import numpy as np
import pandas as pd

class spectrum():
    
    def __init__(self, data_csv, acc_voltage):
        """
        initialize a new spectrum of a potentially unknown composition
        using data from a csv file and the acceleration voltage
        from which it was measured or simulated
        """
        self.spectrum_df = pd.read_csv(data_csv, skiprows=2,
                                     header =None, index_col=[0],
                                     names = ['intensity'])
        self.acc_voltage = acc_voltage


    def quantify(self, lines):
        """
        Will quantify the spectrum output wt% of each element
        by MLLSQ fitting of the selected lines.
        lines should be a list of strings chosen from:
        CuL3M5_6o, GaL3M5_7o, SeL3M5_5o, CuKb,
        GaKa, InL3M1, InL3M5_8o, CuKa, GaKb,
        SeKa, Sekb
        """
        import os
        import os.path
        import statsmodels.formula.api as smf
        import pdb

        folder = 'standards/standard_spectra/'+str(int(self.acc_voltage))+'_keV'
        assert os.path.isdir(folder), 'No standards at desired voltage'

        CIGS = pd.read_csv(folder+'/Standard_Cu100In70Ga30Se200_csv',
                           skiprows=2,
                           header = None, index_col=[0],
                           names = ['intensity'])
        Cu = pd.read_csv(folder+'/Standard_Cu_csv',
                           skiprows=2,
                           header = None, index_col=[0],
                           names = ['intensity'])
        In = pd.read_csv(folder+'/Standard_In_csv',
                           skiprows=2,
                           header = None, index_col=[0],
                           names = ['intensity'])
        Ga = pd.read_csv(folder+'/Standard_Ga_csv',
                           skiprows=2,
                           header = None, index_col=[0],
                           names = ['intensity'])
        Se = pd.read_csv(folder+'/Standard_Se_csv',
                           skiprows=2,
                           header = None, index_col=[0],
                           names = ['intensity'])
        ROI = []# Range of Interest
        for line in lines:
            if line == 'CuL3M5_6o':
                ROI += [[675, 1096]]
                if self.acc_voltage*1000 < 1096:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*1096:
                    print 'Warning: overvoltage < 1.5*ROI'
            elif line == 'GaL3M5_7o':
                ROI += [[829, 1271]]
                if self.acc_voltage*1000 < 1271:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*1271:
                    print 'Warning: overvoltage < 1.5*ROI'
            elif line == 'SeL3M5_5o':
                ROI += [[1089, 1585]]
                if self.acc_voltage*1000 < 1585:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*1585:
                    print 'Warning: overvoltage < 1.5*ROI'
            elif line == 'CuKb':
                ROI += [[8691, 9119]]
                if self.acc_voltage*1000 < 9119:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*9119:
                    print 'Warning: overvoltage < 1.5*ROI'
            elif line == 'GaKa':
                ROI += [[8967, 9520]]
                if self.acc_voltage*1000 < 9520:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*9520:
                    print 'Warning: overvoltage < 1.5*ROI'
            elif line == 'InL3M1':
                ROI += [[2773, 3036]]
                if self.acc_voltage*1000 < 3036:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*3036:
                    print 'Warning: overvoltage < 1.5*ROI'
            elif line == 'InL3M5_8o':
                ROI == [[3085, 4062]]
                if self.acc_voltage*1000 < 4062:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*4062:
                    print 'Warning: overvoltage < 1.5*ROI'
            elif line == 'CuKa':
                ROI += [[7784, 8307]]
                if self.acc_voltage*1000 < 8307:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*8307:
                    print 'Warning: overvoltage < 1.5*ROI'
            elif line == 'GaKb':
                ROI += [[10041, 10487]]
                if self.acc_voltage*1000 < 10487:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*10487:
                    print 'Warning: overvoltage < 1.5*ROI'
            elif line == 'SeKa':
                ROI += [[10911, 11510]]
                if self.acc_voltage*1000 < 11510:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*11510:
                    print 'Warning: overvoltage < 1.5*ROI'
            elif line == 'SeKb':
                ROI += [[12260, 12762]]
                if self.acc_voltage*1000 < 12762:
                    raise Exception, 'line energy < beam energy'
                if self.acc_voltage*1000 < 1.5*12762:
                    print 'Warning: overvoltage < 1.5*ROI'
            else:
                raise Exception, 'Invalid line choice'
        

        data = pd.DataFrame(columns = ('Spec', 'CIGS', 'Cu', 'In', 'Ga', 'Se'))
        #pdb.set_trace()
        for line in ROI:
            line_df = pd.DataFrame({'Spec' : self.spectrum_df.intensity.loc[line[0]:line[1]],#.reindex(np.linspace(line[0],line[1],100),method = 'backfill'),
                                    'CIGS' : CIGS.intensity.loc[line[0]:line[1]],#.reindex(np.linspace(line[0],line[1],100),method = 'backfill'),
                                    'Cu'   : Cu.intensity.loc[line[0]:line[1]],#.reindex(np.linspace(line[0],line[1],100),method = 'backfill'),
                                    'In'   : In.intensity.loc[line[0]:line[1]],#.reindex(np.linspace(line[0],line[1],100),method = 'backfill'),
                                    'Ga'   : Ga.intensity.loc[line[0]:line[1]],#.reindex(np.linspace(line[0],line[1],100),method = 'backfill'),
                                    'Se'   : Se.intensity.loc[line[0]:line[1]]})#.reindex(np.linspace(line[0],line[1],100),method = 'backfill')})
            data = data.append(line_df)
            #pdb.set_trace()
        
        lsq = smf.ols(formula = 'Spec ~ CIGS + Cu + In + Ga + Se - 1', data = data)
        pdb.set_trace()
        coefs = lsq.fit().params
        k_rats = np.array([coefs[0]+coefs[1], 
                           coefs[0]+coefs[2],  
                           coefs[0]+coefs[3], 
                           coefs[0]+coefs[4]])
        wt_frac_std = np.array([0.1969, .2490, .0648, .4893])
        #pdb.set_trace()
        return pd.DataFrame({self.acc_voltage : k_rats*wt_frac_std}, index = ['Cu', 'In', 'Ga', 'Se'])








