# -*- coding: utf-8 -*-
# A script for simulating a homogeneous CIGS layer on Mo and soda-lime glass substrate
# It makes use of the new mcSimulate3 package which provides a simpler mechanism for outputing multiple image and table types from a Monte Carlo simulation.
import dtsa2.mcSimulate3 as mc3
import csv

det = findDetector("Amray") # Replace with your detector's name
# det = findDetector("Si(Li)")
e0 = [5., 10., 15., 16., 17., 18., 19., 20.] # acceleration voltage in [kV]
nTraj = 1000 # number of trajectory for simulation
dose=200 # total amount of electrons [nA*seconds] = probe current [nA] * EDS spectrum process time [seconds]

MW_Ag = 107.8682
MW_Cu = 63.546
MW_In = 114.818
MW_Ga = 69.723
MW_Se = 78.971
MW_S  = 32.06
# get mole fractions

f = open('C:\Users\Lovelett\Dropbox\IEC\dtsa-ii\out2.csv', 'rb')
rows = csv.reader(f)
composition = list(rows)

# convert mole fractions in string to float
for i in range(len(composition)):
    composition[i] = map(float, composition[i])

# convert mole fractions to mass fractions
for line in composition:
	denom = (line[0]*MW_Ag +
		 line[1]*MW_Cu + 
		 line[2]*MW_In + 
		 line[3]*MW_Ga + 
		 line[4]*MW_Se + 
		 line[5]*MW_S)
	line[0] = line[0]*MW_Ag/denom
	line[1] = line[1]*MW_Cu/denom
	line[2] = line[2]*MW_In/denom
	line[3] = line[3]*MW_Ga/denom
	line[4] = line[4]*MW_Se/denom
	line[5] = line[5]*MW_S/denom
#print composition
# define total thickness of CIGS, and other layers
thickness = 3.e-6 # 3 um
Mo_thickness = 0.7e-6 # 0.7 um
SLG_thickness = 2e-3 # 2 mm

layers = []
layercomps = []
i = 0
for line in composition:
    layercomps += [epq.Material(epq.Composition([epq.Element.Ag,
                                                 epq.Element.Cu,
                                                 epq.Element.In,
                                                 epq.Element.Ga,
                                                 epq.Element.Se,
                                                 epq.Element.S],
                                line),
                   epq.ToSI.gPerCC(5.74))]
    #print line
    layercomps[i].setName('(AgCu)(InGa)(SeS)2_'+str(i))
    #print layer
    layers += [[layercomps[i], thickness/len(composition)]]
    i+=1

"""
# Define Cu(In,Ga)Se2
CIS = epq.Material(epq.Composition([epq.Element.Cu,
                                    epq.Element.In,
                                    epq.Element.Se],
                                    [.189, .3414, .4696]),
                   epq.ToSI.gPerCC(5.74)) # need wt. fraction in epq.Composition class!

CIS.setName('CIS')

CGS = epq.Material(epq.Composition([epq.Element.Cu,
                                    epq.Element.Ga,
                                    epq.Element.Se],
                                    [.2182, .2394, .5423]),
                   epq.ToSI.gPerCC(5.61)) # need wt. fraction in epq.Composition class!
CGS.setName('CGS')
"""


Mo = epq.MaterialFactory.createPureElement(epq.Element.Mo)
# Define soda-lime glass
SLG = epq.Material(epq.Composition([epq.Element.Si,
                                     epq.Element.O],
                                     [.467, .533]),
                    epq.ToSI.gPerCC(2.2)) # need wt. fraction in epq.Composition class!
SLG.setName('SLG')
# define layers
layers += [[Mo, Mo_thickness]]
layers += [[SLG, SLG_thickness]] # layers is an iterable list of [material,thickness]
#print layers
def multiFilm(layers, det, e0=20.0, withPoisson=True, nTraj=1000, dose=60.0, sf=False, bf=True, xtraParams={}):
   """
   multiFilm(layers, 
        det, 
		e0=20.0, 
		withPoisson=True, 
		nTraj=defaultNumTraj, 
		dose=defaultDose, 
		sf=defaultCharFluor, 
		bf=defaultBremFluor, 
		xtraParams={}):
   Monte Carlo simulate a spectrum from a multilayer thin film.  Layers is a iterable list of \
   [material,thickness]. Note the materials must have associated densities.
   """
   def buildFilm(monte, chamber, origin, buildParams):
       sr = chamber
       for layer in buildParams["Layers"]:
           if layer[1] <= 0.0:
               raise "The layer thickness must be larger than zero."
           sr = monte.addSubRegion(sr, layer[0], nm.MultiPlaneShape.createSubstrate([0.0, 0.0, -1.0], origin))
           origin = epu.Math2.plus(origin, [0.0, 0.0, layer[1]])
       sr = monte.addSubRegion(sr, epq.Material.Null, nm.MultiPlaneShape.createSubstrate([0.0, 0.0, -1.0], origin))
   #tmp = u"MC simulation of a multilayer film [%s] at %0.1f keV%s%s" % (",".join("%0.2f um of %s" % (1.0e6 * layer[1], layer[0]) for layer in layers), e0, (" + CSF" if sf else ""), (" + BSF" if bf else "")
   tmp = u'Graded_Sample_at_%.1f_keV_%d_layers' % (e0, len(layers))
   return mc3.base(det, e0, withPoisson, nTraj, dose, sf, bf, tmp, buildFilm, {"Layers": layers }, xtraParams)
                      
      

# generation and trajectory images, phi(rho z) curves and other output mechanisms.
tot = 0
for layer in layers:
	tot = tot+layer[1]

for element in e0:
	display(multiFilm(layers,det,e0=element,withPoisson=True,nTraj=nTraj,
	                  dose = dose, sf = False, bf = False,
                          xtraParams={'Trajectories' : thickness,
                                      'TrajSize' : 10**3,
                                       'PhiRhoZ' : thickness,
				       'VRML' : False,
				       'Emission Images' : thickness,
				       'Emission Size' : 10**3,
				       'Transitions' : [epq.XRayTransition(epq.Element.Cu,1),
					                epq.XRayTransition(epq.Element.In,4,2),
							epq.XRayTransition(epq.Element.Ga,1),
							epq.XRayTransition(epq.Element.Se,1),
							epq.XRayTransition(epq.Element.Mo,2)]}))
