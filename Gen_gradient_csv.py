"""
function to generate a csv file for a linear or notched gradient in gallium
"""
import numpy as np

def CSV_gen(min_pos = 0.25,
            Ga_III_surface = .4,
            Ga_III_min = 0.05,
            Ga_III_back = .7,
            N_layers = 100,
            output='out.csv'):

    Cu = np.full(N_layers,0.25)
    Se = np.full(N_layers,0.50)
    
    Ga_III_profile = np.empty(N_layers)
    Ga_III_profile[0:min_pos*N_layers] = np.linspace(
            Ga_III_surface, Ga_III_min, min_pos*N_layers)
    Ga_III_profile[min_pos*N_layers:len(Ga_III_profile)] = np.linspace(
            Ga_III_min, Ga_III_back, 
            (1-min_pos)*N_layers)

    Ga = np.full(N_layers,0.25)*Ga_III_profile
    In = np.full(N_layers,0.25) - Ga
    Ag = np.zeros(N_layers)
    S = np.zeros(N_layers)
    comp = np.column_stack((Ag,Cu,In,Ga,Se,S))
    
    np.savetxt(output, comp, delimiter = ',')

    return None

