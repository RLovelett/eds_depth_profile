# coding: utf-8

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.optimize as spo
import bokeh.plotting as bkp
from bokeh.palettes import brewer
import os.path
import statsmodels.api as sm
import prz_fit_fncs as pr
import patsy
import pdb

#------------------------------
# Analytical Phi Rho Z Functions
#------------------------------
P0_Kfit = sm.load('K_L2_Phi_0.pickle')
Pm_Kfit = sm.load('K_L2_Phi_m.pickle')
zm_Kfit = sm.load('K_L2_z_m.pickle')
zx_Kfit = sm.load('K_L2_z_x.pickle')
P0_Lfit = sm.load('L3_M5_Phi_0.pickle')
Pm_Lfit = sm.load('L3_M5_Phi_m.pickle')
zm_Lfit = sm.load('L3_M5_z_m.pickle')
zx_Lfit = sm.load('L3_M5_z_x.pickle')
def analytical_PRZ(z, e0, Ga_conc,P0_fit, Pm_fit, zm_fit, zx_fit):
    """
    Calcualte Phi given z, e0, Ga atom fraction, and the fit pickles for each parameter at the desired line
    """
    z = np.array(z)
    dmat_dat = patsy.dmatrix('e0 + I(e0**2.) + I(e0**3.) + Ga_conc + I(Ga_conc**2.) + e0:Ga_conc',
                             data = dict({'e0':e0, 'Ga_conc':Ga_conc}))
    #pdb.set_trace()
    return pr.prz(z,P0_fit.predict(dmat_dat, transform = False),
                    Pm_fit.predict(dmat_dat, transform = False),
                    zm_fit.predict(dmat_dat, transform = False),
                    zx_fit.predict(dmat_dat, transform = False))

#---------------------------------------------
# file path constant
#---------------------------------------------
# file path for the unknown sample: PhiRhoZ file and spectrum files
Sample_path = './shallow_notch_ACIGS_3um/'
# file path for the standards: PhiRhoZ file and spectrum files
Std_path = '/Users/leichen/Documents/Python/EDS_simulation/simulation_results/'
# file path for the composition profile file of the simulated sample
profile_path = Sample_path + 'shallow_notch_ACIGS_3um_gradient.csv'
# file path for the quantification file
quant_path = Sample_path + 'quant_shallow_notch_ACIGS_5-30keV_allPeak.csv'

#----------------------------------------
# the quantification profile containing the DTSA quantification results
#----------------------------------------
# import all quantification data from the csv file
df_Ga_quant = pd.read_csv(quant_path)
df_Ga_quant.index = df_Ga_quant['KeV']
df_Ga_quant.index.name = None

KeV = df_Ga_quant.index.values

# energy range
#KeV = [5, 10, 15, 16, 17, 18, 19, 20, 22, 25, 27, 30]
# unknown sample thickness
Thickness = 3.0 # thickness of the CIGS film in micrometer
# composition profile curvature; i.e., linear or polinomial fitting
# n = 1 for linear fitting and n >=2 for nth order polinomial fitting
n = 2
# density of CIGS film in [g/cc]
density = 5.7




# define peak energy range for each element in data frame structure
peak_range = pd.DataFrame(index = ['min', 'max'])
peak_range['[Cu K-L2]'] = np.array([7784, 8307]) # energy in eV
peak_range['[Cu L3-M5]'] = np.array([675, 1096]) # energy in eV
peak_range['[In L3-M5]'] = np.array([3085, 4062]) # energy in eV
peak_range['[Ga K-L2]'] = np.array([8967, 9520]) # energy in eV
peak_range['[Ga L3-M5]'] = np.array([829, 1271]) # energy in eV
peak_range['[Se L3-M5]'] = np.array([1089, 1585]) # energy in eV



# define molecular weight of each element in [g/mol]
wt_Ag = 107.8682
wt_Cu = 63.546
wt_In = 114.82
wt_Ga = 69.723
wt_Se = 78.96
wt_S = 32.065

def Ga_from_wt(Ga_wt_frac):
    return ((0.075*wt_Ag + 0.175*wt_Cu + 0.5*wt_Se + 0.25*wt_In)/
            (wt_Ga/Ga_wt_frac + wt_In - wt_Ga))

#---------------------------------------------
# import PhiRhoZ files
#---------------------------------------------
# PhiRhoZ profile from graded CIGS film: df6 for unknown sample
df6 = pd.DataFrame(index=np.linspace(1, 110, num = 110))
for e0 in KeV:
    df_temp = pd.read_table(Sample_path+'PhiRhoZ_'+str(e0)+'keV.txt')
    df_temp.columns = [df_temp.columns, e0*np.ones(df_temp.shape[1])]
    # skip the 0 row, which is the 2nd header describing the unit of raw and normalized Phi
    df_temp = df_temp.ix[1:]
    # delete the 'Min' and 'Max' column
    del df_temp['Min']
    del df_temp['Max']
    df6=pd.concat([df6, df_temp], axis=1)
# 110 rows of actual data, the z (depth) is from -0.1*3 um to 1.1*3 um, 3 um CIGS notch-graded film
df6.index = np.linspace(-.1*Thickness, 1.1*Thickness, num = 110)


# PhiRhoZ profile from a standard homogeneous CIGS film
df_std = pd.DataFrame(index=np.linspace(1, 110, num = 110))
for e0 in KeV:
    if os.path.isfile(Std_path+'standards_'+str(e0)+'KeV/PhiRhoZ_standard_'+str(e0)+'KeV.csv'):
        df_temp = pd.read_table(Std_path+'standards_'+str(e0)+'KeV/PhiRhoZ_standard_'+str(e0)+'KeV.csv')
        df_temp.columns = [df_temp.columns, e0*np.ones(df_temp.shape[1])]
        # skip the 0 row, which is the 2nd header describing the unit of raw and normalized Phi
        df_temp = df_temp.ix[1:]
        # delete the 'Min' and 'Max' column
        del df_temp['Min']
        del df_temp['Max']
        df_std=pd.concat([df_std, df_temp], axis=1)
df_std.index = np.linspace(-.1*3, 1.1*3, num = 110)



#----------------------------------------
# the composition graded profile
#----------------------------------------
# import Ga profile from csv file
df_profile = pd.read_csv(profile_path, header=None)
df_profile.columns = ['Ag', 'Cu', 'In', 'Ga', 'Se', 'S']
# add Ga [g/cc] column and set calculate its value
df_profile.insert(len(df_profile.columns), 'Ga [g/cc]', 0*np.ones(len(df_profile.index)), allow_duplicates=False)
df_profile['Ga [g/cc]'] = density / (df_profile['Ag'].astype(np.double)*wt_Ag + df_profile['Cu'].astype(np.double)*wt_Cu + df_profile['In'].astype(np.double)*wt_In + df_profile['Ga'].astype(np.double)*wt_Ga + df_profile['Se'].astype(np.double)*wt_Se + df_profile['S'].astype(np.double)*wt_S) * df_profile['Ga'].astype(np.double)*wt_Ga
# calculate the depth at each point
df_profile.insert(len(df_profile.columns), 'depth [um]', Thickness/len(df_profile.index)*(df_profile.index.astype(np.double)+1), allow_duplicates=False)



#---------------------------------------------------
# calculate weighted average depth from PhiRhoZ plot and averaged composition
#---------------------------------------------------
# define the x-ray transition string name for each element; adjust this according to different acceleration voltage
def elemTran(elem, e0):
    # elem: element; string variable, e.g., 'Cu'
    # e0: acceleration voltage
    # return the x-ray transition keywords
    if elem == 'Cu':
        if e0 < 15:
            return '[Cu L3-M5]'
        else:
            return '[Cu K-L2]'
    elif elem == 'In':
        return '[In L3-M5]'
#        if e0 <= 30:
#            return '[In L3-M5]'
#        else:
#            return '[In K-L2]'
    elif elem == 'Ga':
        if e0 < 15:
            return '[Ga L3-M5]'
        else:
            return '[Ga K-L2]'
    elif elem == 'Se':
        return '[Se L3-M5]'
#        if e0 <= 20:
#            return '[Se L3-M5]'
#        else:
#            return '[Se K-L2]'
    elif elem == 'Mo':
        return '[Mo L3-M5]'
    else:
        #print (elem, 'is a non-existing element')
        return ''


# calculate the weighed average depth by averaging polynomial profile
def avgDepth(tran, e0, n, df_PhiRhoZ):
    # tran: the transition keywords for a element
    # e0: acceleration voltage in KeV
    # n: nth power polynominal averaging
    # df_PhiRhoZ: PhiRhoZ dataframe
    #if tran in ['[Cu K-L2]', '[Cu L3-M5]', '[In L3-M5]', '[Ga L3-M5]', '[Ga K-L2]', '[Se L3-M5]', '[Mo L3-M5]']:
    if ('Emit_I'+tran, e0) in df_PhiRhoZ.columns:
        avg = np.trapz(df_PhiRhoZ['Emit_I'+tran, e0].astype(np.double)*np.power(df_PhiRhoZ.index.astype(np.double), n), df_PhiRhoZ.index.astype(np.double)) / np.trapz(df_PhiRhoZ['Emit_I'+tran, e0].astype(np.double), df_PhiRhoZ.index.astype(np.double))
    else:
        avg=0
    return avg


# define simulated composition profile
def simProfile(z, simParam):
    # z: variable representing the depth
    # simParam: ndarray of [m1, m2, zmin, cmin]
    # return c: Ga [g/cc] as a function of depth
    c=0*np.ones( len(z))
    for i in range( len(z)):
        if z[i] <= simParam[2]:
            c[i]=simParam[0]*(z[i]-simParam[2])+simParam[3]
        else:
            c[i]=simParam[1]*(z[i]-simParam[2])+simParam[3]
    return c



def simC(tran, e0, df_PhiRhoZ, simParam):
    # tran: the transition keywords for a element
    # e0: acceleration voltage in KeV
    # df_PhiRhoZ: PhiRhoZ dataframe
    # simParam: parameters for the linear notch profile
    # return calculated Ga [g/cc]
    #if ('Emit_I'+tran, e0) in df_PhiRhoZ.columns:
    #    C_sim = np.trapz(simProfile(df_PhiRhoZ.index.astype(np.double), simParam) * df_PhiRhoZ['Emit_I'+tran, e0].astype(np.double), df_PhiRhoZ.index.astype(np.double)) / np.trapz(df_PhiRhoZ['Emit_I'+tran, e0].astype(np.double), df_PhiRhoZ.index.astype(np.double))
    #else:
    #    C_sim=0
    if tran == '[Ga K-L2]':
        K_PRZ_analytical = np.zeros(len(df_PhiRhoZ))
        #for i, z_cur in enumerate(df_PhiRhoZ.index.astype(np.double)):
        K_PRZ_analytical =  analytical_PRZ(df_PhiRhoZ.index,e0*np.ones(len(df_PhiRhoZ)),simProfile(df_PhiRhoZ.index, simParam), P0_Kfit, Pm_Kfit,zm_Kfit, zx_Kfit)
        df_PhiRhoZ['K_analytical'] = K_PRZ_analytical
        if not np.any(df_PhiRhoZ.K_analytical):
            return 0
        C_sim = np.trapz(simProfile(df_PhiRhoZ.index.astype(np.double), simParam) * df_PhiRhoZ['K_analytical'], df_PhiRhoZ.index.astype(np.double)) / np.trapz(df_PhiRhoZ['K_analytical'], df_PhiRhoZ.index.astype(np.double)) 

    elif tran == '[Ga L3-M5]':
        #pdb.set_trace()
        L_PRZ_analytical = np.zeros(len(df_PhiRhoZ))
        #for i, z_cur in enumerate(df_PhiRhoZ.index.astype(np.double)):
        L_PRZ_analytical =  analytical_PRZ(df_PhiRhoZ.index,e0*np.ones(len(df_PhiRhoZ)),simProfile(df_PhiRhoZ.index, simParam), P0_Lfit, Pm_Lfit,zm_Lfit, zx_Lfit)
        df_PhiRhoZ['L_analytical'] = L_PRZ_analytical
        if not np.any(df_PhiRhoZ.L_analytical):
            return 0
        C_sim = np.trapz(simProfile(df_PhiRhoZ.index.astype(np.double), simParam) * df_PhiRhoZ['L_analytical'], df_PhiRhoZ.index.astype(np.double)) / np.trapz(df_PhiRhoZ['L_analytical'], df_PhiRhoZ.index.astype(np.double)) 
        if np.isnan(C_sim):
            pdb.set_trace()
    else:
        C_sim = 0
    return C_sim


def resid_norm(simParam):
    """
    Given quantification data and parameters, calculates the norm of the residual between the model and the data
    """
    import pdb
    # quant_Ga: Ga [g/cc] from quantification data
    # simParam: parameters for the linear notch profile
    resid = np.ones(len(df_Ga_quant.index))
    for i in range(len(df_Ga_quant.index)):
        e0_i = df_Ga_quant.index[i] # acceleratio voltage for i
        resid[i] = (df_Ga_quant['Ga [g/cc]'].iloc[i] - simC(df_Ga_quant.tran.iloc[i], e0_i, df6, simParam))
    #print np.linalg.norm(resid)
    print simParam
    return np.linalg.norm(resid)


#------------------------------------
# minimize the residual norm, i.e., the difference between the model and data
# params: ndarray of [m1, m2, zmin, cmin]

def avg_comp(params, film_thickness=3.):
    """
    Calculates the average composition of the film given the params:
    params: ndarray of [m1, m2, zmin, cmin]
    and the total film thickness in um
    """
    return (1/film_thickness * (params[0]*params[2]**2/2 - params[0]*params[2]**2
                               +params[1]*film_thickness**2/2 - params[1]*params[2]*film_thickness + params[3]*film_thickness
                               -params[1]*params[2]**2/2 + params[1]*params[2]**2))

deep_notch_params = np.array([(0.1198-0.596)/(.405-0.055), 1, 0.405,0.1198])
#print avg_comp(np.array([-1, 1, .5, .2]))
#print np.mean(df_profile['Ga [g/cc]'])
res = spo.minimize(lambda params : resid_norm(params),
                       #bracket=(0,3),
                       np.array([-.5, 3, .0, 0.5]),
                       method = 'L-BFGS-B',
                       constraints = [#dict({
                                     #'type':'eq',
                                     #'fun':(lambda params : avg_comp(params)-np.mean(df_profile['Ga [g/cc]'].loc[0:3]))
                                     #}),
                                     #dict({
                                     #'type':'eq',
                                     #'fun':(lambda params : -params[0]*params[2]+params[3] - .6625)
                                     #}),
                                     dict({
                                     'type':'ineq',
                                     'fun':(lambda params : 1.425 - params[1]*(Thickness-params[2])+params[3])
                                     }),
                                     dict({
                                     'type':'ineq',
                                     'fun': (lambda params : -params[0])
                                     }),
                                     dict({
                                     'type':'ineq',
                                     'fun':(lambda params : params[1])
                                     })],

                       bounds = [(None, 0), (0, None), (0.,1), (0,None)])


print('the optimization results:')
print(res)
optim_params = res.x

#------------------------------------
# calculate the weighed average of the polynomial profile
# initialize avg_depth and avg_comp data frame
#avg_depth = pd.DataFrame(0.0, index=KeV, columns=pd.MultiIndex.from_product([['Ag', 'Cu', 'In', 'Ga', 'Se', 'S', 'Mo'], np.linspace(n, 0, num=(n+1))]))


#for e0 in KeV:
    # calculate weighted average depth with polynomial profile
#    for elem_i in ['Ag', 'Cu', 'In', 'Ga', 'Se', 'S', 'Mo']:
#        for nth_pw in np.linspace(n, 0, num=(n+1)):
#            pdb.set_trace()
#            avg_depth.set_value(e0, (elem_i, nth_pw), avgDepth(elemTran(elem_i, e0), e0, nth_pw, df6), takeable=False)


# calculate the polynomial coefficient for the polynomial profile
#if (df_Ga_quant.index.values == np.asarray(KeV)).all():
#    coeff = np.linalg.lstsq(avg_depth['Ga'], df_Ga_quant['Ga [g/cc]'])[0]
#else:
#    print('unmatched quantification data and average depth data in KeV')
#    coeff = 0*np.ones(n+1)



#------------------------------------------------------------
# plot the simulated profile
#------------------------------------------------------------
# output to static HTML file
bkp.output_file(Sample_path+'composition profile.html', title='simulated and original profiles')

# create a new plot with a title and axis labels
p_profile = bkp.figure(title='comp. profile', x_axis_label='depth [um]', y_axis_label='Ga [g/cc]', x_range=(0, 3.0), y_range=(0, 1))
# plot the simulated and original composition profile
X_calc = np.linspace(0.0, Thickness, num = 100)
Y_calc_notch = simProfile(X_calc, optim_params)
#Y_calc_poly = np.polyval(coeff, X_calc)
p_profile.line(X_calc, Y_calc_notch, legend='notch sim.', line_color='red', line_width=3)
#p_profile.line(X_calc, Y_calc_poly, legend='poly. sim.', line_color='orange', line_width=3)
#p_profile.circle(avg_depth['Ga', 1], df_Ga_quant['Ga [g/cc]'], line_color='green', fill_color='green', size=6, legend='linear sim.')
p_profile.line(df_profile['depth [um]'], df_profile['Ga [g/cc]'], legend='sample', line_color='blue', line_width=3, line_dash='4 4')

# show the results
bkp.save(p_profile)


#plt.plot(X_calc, Y_calc, label='simulated profile')
#plt.plot(avg_depth['Ga', 1], df_Ga_quant['Ga [g/cc]'], 'ro', label='quant point')
#plt.plot(df_profile['depth [um]'], df_profile['Ga [g/cc]'], color='g', label='sample profile')
#plt.legend()
#plt.ylabel('Ga [g/cc]')
#plt.xlabel('depth ['+r'$\mu$'+'m]')
#plt.show()



#------------------------------------------------------------
# plot the PhiRhoZ of both linear-graded and homogeneous CIGS
#------------------------------------------------------------
#bkp.output_file(Sample_path+'PhiRhoZ.html', title='PhiRhoZ')
#p_prz = bkp.figure(title='PhiRhoZ', x_axis_label='depth [um]', y_axis_label='Intensity [arb. unit]', x_range=(0, 3.0), y_range=(0, 20000))
#for j in range( len(KeV)):
#    e0_j = KeV[j]
#    color_list = brewer["Spectral"][11]+brewer["Spectral"][10]
#    clr = color_list[j]
#    p_prz.line(df6.index.astype(np.double), df6['Emit_I'+elemTran('Ga', e0_j), e0_j].astype(np.double), legend=elemTran('Ga', e0_j)+': notch '+str(e0_j)+'KeV', line_color=clr, line_width=3)

#bkp.save(p_prz)
#for e0 in KeV:
#    df6['Emit_I'+elemTran('Ga', e0), e0].astype(np.double).plot(label = elemTran('Ga', e0)+': notch '+str(e0)+'KeV', legend = True, xlim=[0, 3.5], ylim=[0, 15000])
    #df6['Emit_I'+Cu_tran, e0].astype(np.double).plot(label = Cu_tran+': notch '+str(e0)+'KeV', legend = True, xlim = [0, 3.5])
    #df6['Emit_I'+In_tran, e0].astype(np.double).plot(label = In_tran+': notch '+str(e0)+'KeV', legend = True, xlim = [0, 3.5])
    #df6['Emit_I'+Se_tran, e0].astype(np.double).plot(label = Se_tran+': notch '+str(e0)+'KeV', legend = True, xlim = [0, 3.5])

#plt.plot((In_avg, In_avg), (0, max(df6['Emit_I'+In_tran].astype(np.double))), color = 'm')
#plt.plot((Ga_avg, Ga_avg), (0, max(df6['Emit_I'+Ga_tran].astype(np.double))), color = 'g')
#plt.show()

#ax = df_std['Emit_I'+Cu_tran].astype(double).plot(label = Cu_tran+': linear',legend = True, color = 'b', fontsize = 18,figsize=(15,15), linestyle = 'dashed')
#df_std['Emit_I'+In_tran].astype(np.double).plot(label = In_tran+': std', legend = True, color = 'm', xlim = [0, 1.5], linestyle = 'dashed')
#df_std['Emit_I'+Ga_tran].astype(np.double).plot(label = Ga_tran+': std',legend = True, color = 'g', xlim = [0, 1.5], linestyle='dashed')
#df_std['Emit_I'+Se_tran].astype(double).plot(label = Se_tran+': std', legend = True, color = 'r',xlim = [0, 1.5],linestyle = 'dashed')




